.\" Hey Emacs! This file is -*- nroff -*- source.
.\"
.\" Copyright 1993 Rickard E. Faith (faith@cs.unc.edu)
.\" Copyright 1997 Andries E. Brouwer (aeb@cwi.nl)
.\" Copyright 2009 Samuel Bronson <naesten@gmail.com>
.\"
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of this
.\" manual under the conditions for verbatim copying, provided that the
.\" entire resulting derived work is distributed under the terms of a
.\" permission notice identical to this one.
.\"
.\" Since the Linux kernel and libraries are constantly changing, this
.\" manual page may be incorrect or out-of-date.  The author(s) assume no
.\" responsibility for errors or omissions, or for damages resulting from
.\" the use of the information contained herein.  The author(s) may not
.\" have taken the same level of care in the production of this manual,
.\" which is licensed free of charge, as they might when working
.\" professionally.
.\"
.\" Formatted or processed versions of this manual, if unaccompanied by
.\" the source, must acknowledge the copyright and authors of this work.
.\"
.TH VM86 2 2009-05-07 "Linux" "Linux Programmer's Manual"
.SH NAME
vm86old, vm86 \- enter virtual 8086 mode
.SH SYNOPSIS
.B #include <sys/vm86.h>
.sp
.BI "int vm86old(struct vm86_struct *" info );
.sp
.BI "int vm86(unsigned long " fn ", struct vm86plus_struct *" v86 );
.SH DESCRIPTION
These calls cause the process to enter VM86 mode (virtual-8086 in Intel
literature), and are used by
.BR dosemu (1).
.PP
VM86 mode is an emulation of real mode within a protected mode task.
.PP
.BR vm86 ()
performs one of the following operations based on the function code given in
.IR fn .
.SS Checking Availability
.TP
.B VM86_PLUS_INSTALL_CHECK
For testing if the new
.BR vm86 ()
call is available.
If so, returns 0.
If the architecture is wrong or 
.BR vm86 ()
was left out of the kernel to save a few k, returns \-1 and sets errno
to
.B ENOSYS.
.\" Comments in the source describe using VM86_PLUS_INSTALL_CHECK
.\" also to make sure that it's the *new* vm86 that got called, but
.\" that hardly seems relevant today

.SS IRQ Management
.\" The information in this section was gleaned from a combination of
.\"  * The kernel sources
.\"  * An email from Bart Oldeman
.\"      http://sourceforge.net/mailarchive/message.php?msg_name=c3d607cc0905011152o384ee165t986a96c56b7c868c%40mail.gmail.com
.\"    (at least one other email in this thread was used for other
.\"    portions of what I've added to this page)
.\"  * Dosemu's
.\"      src/arch/linux/async/signal.c
.\"    which Bart pointed me to in the above email
.\" -- Sam
The function codes
.BR VM86_REQUEST_IRQ ", " VM86_FREE_IRQ ", "
.BR VM86_GET_IRQ_BITS ", and " VM86_GET_AND_RESET_IRQ
are used to intercept real hardware IRQs from interrupt vectors 3
through 15.
They treat the second argument to
.BR vm86()
as an integer;
the rest of this subsection will act as if it were declared as
.BI "unsigned\ long\ " irqnumber \fR.
.LP
The
.B VM86_REQUEST_IRQ
function code requires the
.B CAP_SYS_ADMIN
capability
(superuser for Linux versions before the introduction of
capabilities in 2.1.100);
since none of the other function codes actually do anything without
being preceded by a successful
.BR VM86_REQUEST_IRQ ,
this effectively limits the entire facility to programs running with
that capability.
.LP
This facility actually has little to do with VM86 mode;
you can use it independantly to e.g. implement hardware drivers in
userspace.
The main thing the two have in common is that 
.BR dosemu (1)
can use them both, which is probably why this facility included in this
system call rather than implemented as a seperate system call.

.TP
.B VM86_REQUEST_IRQ
Arrange for hardware interrupt
.I irq
to be delivered as signal
.IR sig ,
where

.in +4n
.IR irqnumber "\ =\ " sig "\ <<\ 8 +\ " irq .
.in

.I sig
may be one of
.BR SIGUSR1 ", "
.BR SIGUSR2 ", "
.BR SIGIO ", "
.BR SIGURG ", "
.BR SIGUNUSED ","
or, if no signal should be generated for this IRQ, 0.

Upon receipt of this interrupt, the kernel will send the requested
signal (if any) to the requesting process and disable the interrupt
pending a
.B VM86_GET_AND_RESET_IRQ
from that process.
The kernel may become displeased if the interrupt is not handled in a
timely fashion.

.TP
.B VM86_FREE_IRQ
End delivery of interrupt
.I irqnumber
to this process, and allow other processes to request it.

.TP
.B VM86_GET_IRQ_BITS
Returns a bitmap where 1 <<
.I intno
is set if interrupt
.I intno
has been recieved on behalf of, but not handled by,
a process that requested it with
.BR VM86_REQUEST_IRQ .

.TP
.B VM86_GET_AND_RESET_IRQ
Check if interrupt
.I irqnumber
since the last time either
.B VM86_REQUEST_IRQ
or this function code was called on it
and, if so, clear it's bit in the bitmap returned by
.BR VM86_GET_IRQ_BITS
and re-enable the IRQ.

The above assumes that the calling process has requested the given
interrupt with
.B VM86_REQUEST_IRQ
without having subsequently released it with
.BR VM86_FREE_IRQ .
If this is not the case, this function code just returns 0.

.SS Entering VM86 mode
The
.B VM86_ENTER
and
.B VM86_ENTER_NO_BYPASS
function codes are used to enter VM86 mode.
They use the passed
.I "struct\ vm86plus_struct"
both to control operation and to maintain the virtual machine's
register values.
This structure is defined as follows:
.ft CR
.in +4n
.nf

struct vm86_regs {
/*
 * normal regs, with special meaning for the segment descriptors..
 */
	long ebx;
	long ecx;
	long edx;
	long esi;
	long edi;
	long ebp;
	long eax;
	long __null_ds;
	long __null_es;
	long __null_fs;
	long __null_gs;
	long orig_eax;
	long eip;
	unsigned short cs, __csh;
	long eflags;
	long esp;
	unsigned short ss, __ssh;

/*
 * these are specific to v86 mode:
 */
	unsigned short es, __esh;
	unsigned short ds, __dsh;
	unsigned short fs, __fsh;
	unsigned short gs, __gsh;
};

struct revectored_struct {
	unsigned long __map[8];			/* 256 bits */
};

struct vm86plus_info_struct {
	unsigned long force_return_for_pic:1;
	unsigned long vm86dbg_active:1;       /* for debugger */
	unsigned long vm86dbg_TFpendig:1;     /* for debugger */
	unsigned long unused:28;
	unsigned long is_vm86pus:1;           /* for vm86 internal use */
	unsigned char vm86dbg_intxxtab[32];   /* for debugger */
};
struct vm86plus_struct {
	struct vm86_regs regs;
	unsigned long flags;
	unsigned long screen_bitmap;
	unsigned long cpu_type;
	struct revectored_struct int_revectored;
	struct revectored_struct int21_revectored;
	struct vm86plus_info_struct vm86plus;
};

.in
.fi
The
.I vm86_regs
structure holds the register values of the virtual machine.
The
.IR __null_ds ", " __null_es ", " __null_fs ", and " __null_gs
fields are generally zero, since the CPU clears these registers
(after saving them to the stack)
when leaving VM86 mode in order to allow the OS to safely save and
restore these registers in interrupt/exception handlers regardless of
whether the interrupted process was running in VM86 mode \(em it is not
safe to load arbitrary values into these registers in protected mode.
The real segment register values are kept in fields
.IR es ", " ds ", " fs ", and " gs .
The CS and SS segment registers, on the other hand, get pushed on the
kernel stack on exit from VM86 mode "before" being loaded with new
values from the IDT and TSS, respectively.
(Not
.B quite
before, since the CPU needs to use the new SS to do the push, but it
is the old value that is pushed.)
Because valid protected-mode values for these registers are loaded by
the CPU, there is no
.IR __null_cs " or " __null_ss
field.

.PP
FIXME: Document more of the fields.

.SH "RETURN VALUE"
The return value of a successful
.BR vm86old ()
call consists of a type code, accessed using the macro
.BR VM86_TYPE "(\fIretval\fP),"
and an argument, accessed using the macro
.BR VM86_ARG "(\fIretval\fP)."
The type code is one of the following:
.TP
.BR "VM86_SIGNAL = 0" " (no argument)"
The process left VM86 mode because it recieved a signal.
.TP
.BR VM86_UNKNOWN " (no argument)"
There was a GP fault that the kernel did not not know how to handle.
.\" There is presumably no exception error code, since that is only
.\" relevent for GP faults when triggered by a bad segment selector
.TP
.BR VM86_INTx " (interrupt number)"
The VM encountered an INT instruction.
CS:IP points to the following instruction.
.TP
.BR VM86_STI " (no argument)"
An instruction (sti/popf/iret) enabled interrupts for the VM, and
the VIP (virtual interrupt pending) flag was set in EFLAGS.
(Neither the kernel nor the CPU set VIP directly, so this can only
happen if it is set by the application in the passed structure.
Also, as of
.IR v2.6.30-rc4 ,
the kernel still does not actually use the VME extension in which VIP
is defined to implement this, though that shouldn't affect the behaviour.)
.PP
The return value of a successful
.BR vm86 ()
call depends on the function code:
.TP
.B VM86_PLUS_INSTALL_CHECK
Zero.
.TP
.B VM86_ENTER
As for
.BR vm86old (),
except that the following additional return types are possible:
.RS
.TP
.BR VM86_PICRETURN " (no argument)"
The VM encountered a PUSHF, POPF, IRET, STI, or CLI instruction, and the
.I v86->vm86plus.force_return_for_pic
field was set.
.TP
.BR VM86_TRAP " (exception number)"
The VM generated exception number 1 (DB \(en debug exception) or 3 (BP
\(en breakpoint).
.RE

.LP
On error, \-1 is returned, and
.I errno
is set appropriately.
.SH ERRORS
.TP
.B EFAULT
This return value is specific to i386 and indicates a problem with getting
userspace data.
.TP
.B ENOSYS
This return value indicates the call is not implemented on the present
architecture (or that the kernel was built without support for it).
.TP
.B EPERM
Saved kernel stack exists.
(This is a kernel sanity check; the saved
stack should only exist within vm86 mode itself.)
.SH "VERSIONS"
The system call
.BR vm86 ()
was introduced in Linux 0.97p2.
In Linux 2.1.15 and 2.0.28 it was renamed to
.BR vm86old (),
and a new
.BR vm86 ()
was introduced.
The definition of \fIstruct vm86_struct\fP was changed
in 1.1.8 and 1.1.9.
.SH "CONFORMING TO"
This call is specific to Linux on 32-bit Intel processors,
and should not be used in programs intended to be portable.
.SH "SEE ALSO"
.BR capabilities (7),
\fIIA-32 Intel\*[R] Architecture Software Developer's Manual\fP
