.\" Copyright (C) 2007 Michael Kerrisk <mtk.manpages@gmail.com>
.\" and Copyright (c) 1993 by Thomas Koenig (ig25@rz.uni-karlsruhe.de)
.\"
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of this
.\" manual under the conditions for verbatim copying, provided that the
.\" entire resulting derived work is distributed under the terms of a
.\" permission notice identical to this one.
.\"
.\" Since the Linux kernel and libraries are constantly changing, this
.\" manual page may be incorrect or out-of-date.  The author(s) assume no
.\" responsibility for errors or omissions, or for damages resulting from
.\" the use of the information contained herein.  The author(s) may not
.\" have taken the same level of care in the production of this manual,
.\" which is licensed free of charge, as they might when working
.\" professionally.
.\"
.\" Formatted or processed versions of this manual, if unaccompanied by
.\" the source, must acknowledge the copyright and authors of this work.
.\" License.
.\" Modified Sat Jul 24 18:34:44 1993 by Rik Faith (faith@cs.unc.edu)
.\" Merged readv.[23], 2002-10-17, aeb
.\" 2007-04-30 mtk, A fairly major rewrite to fix errors and
.\"     add more details.
.\"
.TH READV 2  2002-10-17 "Linux" "Linux Programmer's Manual"
.SH NAME
readv, writev \- read or write data into multiple buffers
.SH SYNOPSIS
.nf
.B #include <sys/uio.h>
.sp
.BI "ssize_t readv(int " fd ", const struct iovec *" iov ", int " iovcnt );
.sp
.BI "ssize_t writev(int " fd ", const struct iovec *" iov ", int " iovcnt );
.fi
.SH DESCRIPTION
The
.BR readv ()
function reads
.I iovcnt
buffers from the file associated with the file descriptor
.I fd
into the buffers described by
.I iov
("scatter input").
.PP
The
.BR writev ()
function writes
.I iovcnt
buffers of data described by
.I iov
to the file associated with the file descriptor
.I fd
("gather output").
.PP
The pointer
.I iov
points to an array of
.I iovec
structures,
defined in
.I <sys/uio.h>
as:
.PP
.br
.in +4n
.nf
struct iovec {
    void  *iov_base;    /* Starting address */
    size_t iov_len;     /* Number of bytes to transfer */
};
.fi
.in
.PP
The
.BR readv ()
function works just like
.BR read (2)
except that multiple buffers are filled.
.PP
The
.BR writev ()
function works just like
.BR write (2)
except that multiple buffers are written out.
.PP
Buffers are processed in array order.
This means that
.BR readv ()
completely fills
.IR iov [0]
before proceeding to
.IR iov [1],
and so on.
(If there is insufficient data, then not all buffers pointed to by
.I iov
may be filled.)
Similarly,
.BR writev ()
writes out the entire contents of
.IR iov [0]
before proceeding to
.IR iov [1],
and so on.
.PP
The data transfers performed by
.BR readv ()
and
.BR writev ()
are atomic: the data written by
.BR writev ()
is written as a single block that is not intermingled with output
from writes in other processes (but see
.BR pipe (7)
for an exception);
analogously,
.BR readv ()
is guaranteed to read a contiguous block of data from the file,
regardless of read operations performed in other threads or processes
that have file descriptors referring to the same open file description
(see
.BR open (2)).
.SH "RETURN VALUE"
On success, the
.BR readv ()
function returns the number of bytes read; the
.BR writev ()
function returns the number of bytes written.
On error, \-1 is returned, and \fIerrno\fP is set appropriately.
.SH ERRORS
The errors are as given for
.BR read (2)
and
.BR write (2).
Additionally the following error is defined:
.TP
.B EINVAL
The sum of the
.I iov_len
values overflows an
.I ssize_t
value.
Or, the vector count \fIiovcnt\fP is less than zero or greater than the
permitted maximum.
.SH "CONFORMING TO"
4.4BSD (the
.BR readv ()
and
.BR writev ()
functions first appeared in 4.2BSD), POSIX.1-2001.
Linux libc5 used \fIsize_t\fP as the type of the \fIiovcnt\fP argument,
and \fIint\fP as return type for these functions.
.\" The readv/writev system calls were buggy before Linux 1.3.40.
.\" (Says release.libc.)
.SH NOTES
.SS Linux Notes
POSIX.1-2001 allows an implementation to place a limit on
the number of items that can be passed in
.IR iov .
An implementation can advertise its limit by defining
.B IOV_MAX
in
.I <limits.h>
or at run time via the return value from
.IR sysconf(_SC_IOV_MAX) .
On Linux, the limit advertised by these mechanisms is 1024,
which is the true kernel limit.
However, the glibc wrapper functions do some extra work if
they detect that the underlying kernel system call failed because this
limit was exceeded.
In the case of
.BR readv ()
the wrapper function allocates a temporary buffer large enough
for all of the items specified by
.IR iov ,
passes that buffer in a call to
.BR read (2),
copies data from the buffer to the locations specified by the
.I iov_base
fields of the elements of
.IR iov ,
and then frees the buffer.
The wrapper function for
.BR writev ()
performs the analogous task using a temporary buffer and a call to
.BR write (2).
.SH BUGS
It is not advisable to mix calls to functions like
.BR readv ()
or
.BR writev (),
which operate on file descriptors, with the functions from the stdio
library; the results will be undefined and probably not what you want.
.SH EXAMPLE
The following code sample demonstrates the use of
.BR writev ():

.in +4n
.nf
char *str0 = "hello ";
char *str1 = "world\\n";
struct iovec iov[2];
ssize_t nwritten;

iov[0].iov_base = str0;
iov[0].iov_len = strlen(str0);
iov[1].iov_base = str1;
iov[1].iov_len = strlen(str1);

nwritten = writev(STDOUT_FILENO, iov, 2);
.fi
.in
.SH "SEE ALSO"
.BR read (2),
.BR write (2)
